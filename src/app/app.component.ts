import { Component } from '@angular/core';
import { data } from '../app/json'
import { MainService } from '../app/main.service'
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public allData : any ;
  public keysArray = []; 
  public valueArray = []; 
  public finalValue :any;
  public country2 : any;
  public titleForm: FormGroup;
  public secondForm: FormGroup;
  public lengthForm: FormGroup;
  public secondlengthForm: FormGroup;
  public finalLengthValue : any;
  public lastLength :any;
  public finalUnit : any ;
  public lengthArray: string[] = ['Yard', 'Meter', 'Inch', 'Foot' ]
  submitted:boolean = false;
  submit:boolean = false;

  constructor(private mainService : MainService , private _fb: FormBuilder, private toastrService : ToastrService)
  {
    this.titleForm = this._fb.group({
      number1: [null, [Validators.required , Validators.pattern('^[0-9]*$')]],
      countryOne: ['USD', [Validators.required]],

    });
    this.secondForm = this._fb.group({
      countryTwo: ['INR', [Validators.required]],

    });

    this.lengthForm = this._fb.group({
      value1: [null, [Validators.required , Validators.pattern('^[0-9]*$')]],
      lengthOne: ['Yard', [Validators.required]],

    });

    this.secondlengthForm = this._fb.group({
      lengthTwo: ['Meter', [Validators.required]],
    });

  }

  ngOnInit() {
    this.data();
    
  }
  get f() { return this.titleForm.controls; }

  get g(){ return this.lengthForm.controls}

  data(){
    let data = this.mainService.data()
    this.keysArray = Object.keys(data.rates)
    this.valueArray = Object.values(data.rates)

  }

  submitCurrencyConvetor(){

    this.submitted=true; 
    if (this.titleForm.invalid) {
      return this.toastrService.error('Please Type Number')
  }

    let number1 = this.titleForm.value.number1
    let country1 = this.titleForm.value.countryOne
    this.country2 = this.secondForm.value.countryTwo
    let indexOfFirstValue = this.keysArray.findIndex( value => value == country1);
    let indexOfSecondValue = this.keysArray.findIndex( value => value == this.country2);
    let valueOfFirst = this.valueArray[indexOfFirstValue]
    let valueOfSecond = this.valueArray[indexOfSecondValue]
    let value = (number1 / valueOfFirst) * valueOfSecond
    this.finalValue = value.toFixed(3)


  }

  submitLengthConvertor(){
    this.submit=true; 
    if (this.lengthForm.invalid) {
      return this.toastrService.error('Please Type Number')
  }
    let firstValue = this.lengthForm.value.value1
    let secondVlaue = this.secondlengthForm.value.value2
    let firstlenthValue = this.lengthForm.value.lengthOne
    let secondlengthVlaue = this.secondlengthForm.value.lengthTwo 

    if(firstlenthValue == 'Yard' && secondlengthVlaue == 'Meter'){
      this.lastLength = firstValue / 1.094
      this.finalUnit = 'm'
    }else if (firstlenthValue == 'Yard' && secondlengthVlaue == 'Inch'){
      this.lastLength = firstValue * 36
      this.finalUnit = 'in'
    }else if (firstlenthValue == 'Yard' && secondlengthVlaue == 'Foot'){
      this.lastLength = firstValue * 3
      this.finalUnit = 'Foot'
    }else if (firstlenthValue == 'Meter' && secondlengthVlaue == 'Yard'){
      this.lastLength = firstValue * 1.094
      this.finalUnit = 'yd'
    }else if (firstlenthValue == 'Meter' && secondlengthVlaue == 'Inch'){
      this.lastLength = firstValue * 39.37
      this.finalUnit = 'in'
    }else if (firstlenthValue == 'Meter' && secondlengthVlaue == 'Foot'){
      this.lastLength = firstValue * 3.281
      this.finalUnit = 'Foot'
    }else if (firstlenthValue == 'Inch' && secondlengthVlaue == 'Yard'){
      this.lastLength = firstValue / 36
      this.finalUnit = 'yd'
    }else if (firstlenthValue == 'Inch' && secondlengthVlaue == 'Meter'){
      this.lastLength = firstValue / 39.37
      this.finalUnit = 'm'
    }else if (firstlenthValue == 'Inch' && secondlengthVlaue == 'Foot'){
      this.lastLength = firstValue / 12
      this.finalUnit = 'Foot'
    }else if (firstlenthValue == 'Foot' && secondlengthVlaue == 'Yard'){
      this.lastLength = firstValue / 3
      this.finalUnit = 'yd'
    }else if (firstlenthValue == 'Foot' && secondlengthVlaue == 'Meter'){
      this.lastLength = firstValue / 3.281
      this.finalUnit = 'm'
    } else if (firstlenthValue == 'Foot' && secondlengthVlaue == 'Inch'){
      this.lastLength = firstValue * 12
      this.finalUnit = 'in'
    } else if (firstlenthValue == 'Yard' && secondlengthVlaue == 'Yard'){
      this.lastLength = firstValue
      this.finalUnit = 'yd'
    } else if (firstlenthValue == 'Foot' && secondlengthVlaue == 'Foot'){
      this.lastLength = firstValue 
      this.finalUnit = 'Foot'
    } else if (firstlenthValue == 'Inch' && secondlengthVlaue == 'Inch'){
      this.lastLength = firstValue 
      this.finalUnit = 'in'
    }

    this.finalLengthValue = this.lastLength.toFixed(3)

  }


  lengthInput(value){ 
    if(!this.titleForm.value.number1){ 
      this.finalValue = '';
      this.country2 = '' ;
      }
  }

  lengthOfLengthConvertor(){ 
    if(!this.lengthForm.value.value1){ 
      this.finalLengthValue = '' ;
      this.finalUnit = '' ;
    }
  }
}
