import { Injectable } from '@angular/core';
import { data } from '../app/json'

@Injectable({
  providedIn: 'root'
})
export class MainService {

  constructor() { }

  data(){
    return data
  }
}
